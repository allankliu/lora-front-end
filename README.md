# Readme

This is an archive for my previous LoRa USB dongle. After working for a mesh network for three years, I feel I can 
convert it as an open source project and promote it as easy project for everyone. 

I plan to prmote the legacy design (LoRa Ping Pong) into different directions.

1. LoRa PHY driver and peer to peer Ping Pong USB dongle with GCC/Keil
2. LoRa star toplogy over serial dongle
3. LoRa tree toplogy over serial dongle with limited TTL and bandwidth allocation
4. 6LowPAN over LoRa star and LoRa tree/mesh
5. Mist computing algorithm between nodes
6. New PHY with Sub-1GHz on Chinese radio modules
7. New PHY with IEEE802.15.4
8. New PHY with IEEE802.11ax
